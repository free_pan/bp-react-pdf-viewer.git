import React, {useState} from 'react';
import {Meta} from '@storybook/react';
import {PdfViewerByPage} from '../src/lib/PdfViewerByPage';

export default {
    component: PdfViewerByPage,
    title: '示例/PdfViewerByPage',
    argTypes: {
        getTotalPage: {action: 'getTotalPage '}
    },
} as Meta;

export const 所有参数 = (args: any) => <PdfViewerByPage {...args} />;
所有参数.args = {
    cMapUrl: 'https://unpkg.com/pdfjs-dist@2.6.347/cmaps/',
    pdfUrl: 'https://arxiv.org/pdf/quant-ph/0410100.pdf',
    pageNum: 1
};


export const 自定义分页 = () => {
    const [totalPage, setTotalPage] = useState<number>(0);
    const [pageNum, setPageNum] = useState<number>(1);
    const onPageChange = (incNum: number) => {
        setPageNum(prev => {
            const ret = prev + incNum;
            if (ret === 0) {
                return prev;
            }
            if (ret > totalPage) {
                return prev;
            }
            return ret;
        });
    }
    const onGetTotalPage = (tp: number) => {
        console.log('tp', tp)
        setTotalPage(tp);
    }
    return (
        <div>
            <button onClick={() => onPageChange(-1)}>上一页</button>
            页码: {pageNum}/{totalPage}
            <button onClick={() => onPageChange(1)}>下一页</button>
            <PdfViewerByPage cMapUrl={'https://unpkg.com/pdfjs-dist@2.6.347/cmaps/'}
                             pdfUrl="https://arxiv.org/pdf/quant-ph/0410100.pdf" pageNum={pageNum}
                             getTotalPage={onGetTotalPage}/>
        </div>
    )
};
