import React from 'react';
import {Meta} from '@storybook/react';
import PdfViewer from '../src/lib/PdfViewer';

export default {
    component: PdfViewer,
    title: '示例/PdfViewer',
} as Meta;

export const 所有参数 = (args: any) => <PdfViewer {...args} />;
所有参数.args = {
    cMapUrl: 'https://unpkg.com/pdfjs-dist@2.6.347/cmaps/',
    pdfUrl: 'https://arxiv.org/pdf/quant-ph/0410100.pdf',
};
export const 基本使用 = () => <PdfViewer cMapUrl={'https://unpkg.com/pdfjs-dist@2.6.347/cmaps/'}
                                     pdfUrl="https://arxiv.org/pdf/quant-ph/0410100.pdf"/>;
