import 'react-app-polyfill/ie11';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {PdfViewer, PdfViewerByPage} from '../.';
import {useState} from "react";

const App = () => {
    const [scale, setScale] = useState<number>(1);
    const [totalPage, setTotalPage] = useState<number>(0);
    const [pageNum, setPageNum] = useState<number>(1);
    const onPageChange = (incNum) => {
        setPageNum(prev => {
            const ret = prev + incNum;
            if (ret === 0) {
                return prev;
            }
            if (ret > totalPage) {
                return prev;
            }
            return ret;
        });
    }
    const onPlusScale = () => {
        setScale(prev => prev + 0.5);
    }
    const onReduceScale = () => {
        setScale(prev => prev - 0.5);
    }
    const onGetTotalPage = (tp: number) => {
        console.log('tp', tp)
        setTotalPage(tp);
    }
    return (
        <div>
            <h2>分页显示pdf</h2>
            <button onClick={() => onPageChange(-1)}>上一页</button>
            页码: {pageNum}/{totalPage}
            <button onClick={() => onPageChange(1)}>下一页</button>
            <PdfViewerByPage pdfUrl={'http://localhost:1234/test.pdf'} pageNum={pageNum}
                             getTotalPage={onGetTotalPage}></PdfViewerByPage>
            <h2>完整显示pdf</h2>
            <button onClick={onPlusScale}>增加缩放比</button>
            <button onClick={onReduceScale}>减少缩放比</button>
            <PdfViewer pdfUrl={'http://localhost:1234/test.pdf'} scale={scale}></PdfViewer>
        </div>
    );
};

ReactDOM.render(<App/>, document.getElementById('root'));
