const copy=require('rollup-plugin-copy')
const postcss = require('rollup-plugin-postcss');
const autoprefixer = require('autoprefixer');

module.exports = {
    rollup(config, options) {
        config.plugins.push(
            postcss({
                plugins: [
                    autoprefixer(),
                ],
                inject: false, // 是否主动注入css
                extract: !!options.writeMeta,
                camelCase: true, // 支持驼峰
                sass: true, // 是否使用sass
                less: true, // 是否使用less
            }),
            copy({
                targets: [
                    { src: 'src/comm.scss', dest: 'dist/' },
                    // 加入copy支持,为后续按需加载样式做准备
                    { src: 'src/lib/button/index.scss', dest: 'dist/lib/button/' },
                ]
            })
        );
        return config;
    },
};
